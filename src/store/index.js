import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const STORAGE_KEY = 'awesome-vue-todo';

const state = {
  todos: JSON.parse(window.localStorage.getItem(STORAGE_KEY+'-store') || '[]'),
  increment: JSON.parse(window.localStorage.getItem(STORAGE_KEY+'-increment') || '0'),
}

const getters = {
  arrId (state) {
    return state.todos.map(todo => todo.id);
  },
  maped (state) {
    let objTodo = {};
    state.todos.forEach(item => {
      objTodo[item.id] = item; 
    });
    return objTodo;
  },
};

const mutations = {
  increment (state) {
    state.increment += 1;
  },
  addTodo (state, todo) {
    state.todos.push(todo);
  },
  removeTodo (state, todo) {
    state.todos.splice(state.todos.indexOf(todo), 1);
  },
  editTodo (state, { todo, title = todo.title, description = todo.description, done = todo.done, date = todo.date }) {
    todo.title = title;
    todo.description = description;
    todo.done = done;
    todo.date = date;
  },
}

const actions = {
  addTodo ({ commit, state }, { title, description }) {
    commit('increment');
    const d = new Date;
    commit('addTodo', {
      id: state.increment,
      title,
      description,
      date: `${d.getFullYear()}-${d.getMonth()}-${d.getDate()} ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`,
      done: false,
    })
  },
  removeTodo ({ commit }, todo) {
    commit('removeTodo', todo);
  },
  toggleTodo ({ commit }, todo) {
    commit('editTodo', { todo, done: !todo.done });
  },
  editTodo ({ commit, state }, value) {
    const { id, title, description, data } = value;
    const todo = state.todos.find(todo => todo.id ==id);
    commit('editTodo', { todo, title, description, data });
  },
}

const plugins = [store => {
  store.subscribe((mutation, { todos, increment }) => {
    window.localStorage.setItem(STORAGE_KEY+'-store', JSON.stringify(todos));
    window.localStorage.setItem(STORAGE_KEY+'-increment', JSON.stringify(increment));
  })
}]

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  plugins,
})
