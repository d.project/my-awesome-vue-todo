import Vue from 'vue';
import Router from 'vue-router';
// import store from '@/store/index.js';
import Main from '@/pages/index.vue';
import NewTask from '@/pages/new-task.vue';
import TaskDetail from '@/pages/task.vue';

Vue.use(Router);

const router = new Router({
  scrollBehavior: () => ({ y: 0 }),
  //mode: 'abstract',
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main,
    },
    {
      path: '/new-task',
      name: 'NewTask',
      component: NewTask,
    },
    {
      path: '/task/:id',
      name: 'TaskDetail',
      component: TaskDetail,
      props: true,
    },
  ]
})

// router.beforeEach((to, from, next) => {
//   if (store.getters.arrId.some(record => record === to.params.id)) {
//     next()
//   } else {
//     next('/')
//   }
// })

export default router
